"""Main application file"""
from flask import Flask

MESSAGE = "Hello world, Maxim Chepukov"

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    """Return message when ping to root route"""
    return MESSAGE


if __name__ == "__main__":
    # Start flask application
    app.run(host="0.0.0.0", debug=False, port=5000)  # nosec
