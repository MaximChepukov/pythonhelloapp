# Exam task
This is exam task for Andersen DevOps Course

## Short description
* Python app than should return "Hello world 2" by http request
* Application should be checked and deployed by CI\CD process


## Short info about implementation

This application deploy to Amazon ECS (Elastic Container Service) and can be checked by request

```shell
curl http://ec2-18-189-188-247.us-east-2.compute.amazonaws.com && echo
# It should return something like this
Hello world 2
```